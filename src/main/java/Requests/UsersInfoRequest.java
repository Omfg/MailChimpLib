package Requests;

import Interfaces.InfoInterface;
import Objects.AuthObj;
import Objects.UserInfoResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.*;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsersInfoRequest {


    public void usersInfoRequest(String baseUrl,AuthObj authObj){
//
//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor).build();
//
//

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://"+baseUrl)
//                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        InfoInterface infoInterface = retrofit.create(InfoInterface.class);
        Call<UserInfoResponse> request = infoInterface.getInfo(authObj);

        request.enqueue(new Callback<UserInfoResponse>() {
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                if(response.isSuccessful()){
                    UserInfoResponse userInfoResponse = new UserInfoResponse();
                    System.out.println(userInfoResponse.getUsername());
                }else {
                    System.out.println(response.code());
                }
            }

            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
t.printStackTrace();
                System.out.println(t);
            }
        });
    }
}
