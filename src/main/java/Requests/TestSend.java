package Requests;

import Objects.MandrillCredentials;
import Interfaces.Messages.MessageSenderInterface;
import Interfaces.SenderInterface;
import Objects.Messages.SendResponse.MessageSendResponse;
import Objects.Messages.Send_Request.SendClass;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.ArrayList;
import java.util.List;

public class TestSend implements SenderInterface{


    public void send(MandrillCredentials mandrillCredentials, SendClass sendClass) {
        HttpLoggingInterceptor httpLoggingInterceptor  = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor).build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mandrillCredentials.getBaseUrl())
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final MessageSenderInterface messageSenderInterface = retrofit.create(MessageSenderInterface.class);

        Call<List<MessageSendResponse>> request = messageSenderInterface.getMessageSend(sendClass);

        request.enqueue(new Callback<List<MessageSendResponse>>() {
            public void onResponse(Call<List<MessageSendResponse>> call, Response<List<MessageSendResponse>> response) {

                if (response.isSuccessful()){

                    List<MessageSendResponse> messageSendResponses = new ArrayList<MessageSendResponse>();
                    for (int i = 0; i <messageSendResponses.size() ; i++) {


                        MessageSendResponse messageSendResponse = new MessageSendResponse();
                        System.out.println(messageSendResponse.getEmail());
                        System.out.println(messageSendResponse.getId());
                        messageSendResponses.add(messageSendResponse);
                    }

                }else {
                    System.out.println(response.code());
                }
            }


            public void onFailure(Call<List<MessageSendResponse>> call, Throwable throwable) {
throwable.printStackTrace();

            }
        });

    }
}
