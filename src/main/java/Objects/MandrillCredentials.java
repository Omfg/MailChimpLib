package Objects;

public class MandrillCredentials {
    private String apiKey;
    private String baseUrl;

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {

        return baseUrl;
    }

    public String getApiKey() {

        return apiKey;
    }
}
