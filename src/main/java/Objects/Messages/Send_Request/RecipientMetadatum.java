
package Objects.Messages.Send_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecipientMetadatum {

    @SerializedName("rcpt")
    @Expose
    private String rcpt;
    @SerializedName("values")
    @Expose
    private Values values;

    public String getRcpt() {
        return rcpt;
    }

    public void setRcpt(String rcpt) {
        this.rcpt = rcpt;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }

}
