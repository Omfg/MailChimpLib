
package Objects.Messages.Send_Request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MergeVar {

    @SerializedName("rcpt")
    @Expose
    private String rcpt;
    @SerializedName("vars")
    @Expose
    private List<Var> vars = null;

    public String getRcpt() {
        return rcpt;
    }

    public void setRcpt(String rcpt) {
        this.rcpt = rcpt;
    }

    public List<Var> getVars() {
        return vars;
    }

    public void setVars(List<Var> vars) {
        this.vars = vars;
    }

}
