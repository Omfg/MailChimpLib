
package Objects.Messages.Send_Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendClass {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("async")
    @Expose
    private Boolean async;
    @SerializedName("ip_pool")
    @Expose
    private String ipPool;
    @SerializedName("send_at")
    @Expose
    private String sendAt;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Boolean getAsync() {
        return async;
    }

    public void setAsync(Boolean async) {
        this.async = async;
    }

    public String getIpPool() {
        return ipPool;
    }

    public void setIpPool(String ipPool) {
        this.ipPool = ipPool;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

}
