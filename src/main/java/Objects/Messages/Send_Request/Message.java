
package Objects.Messages.Send_Request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("html")
    @Expose
    private String html;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("from_email")
    @Expose
    private String fromEmail;
    @SerializedName("from_name")
    @Expose
    private String fromName;
    @SerializedName("to")
    @Expose
    private List<To> to = null;
    @SerializedName("headers")
    @Expose
    private Headers headers;
    @SerializedName("important")
    @Expose
    private Boolean important;
    @SerializedName("track_opens")
    @Expose
    private Object trackOpens;
    @SerializedName("track_clicks")
    @Expose
    private Object trackClicks;
    @SerializedName("auto_text")
    @Expose
    private Object autoText;
    @SerializedName("auto_html")
    @Expose
    private Object autoHtml;
    @SerializedName("inline_css")
    @Expose
    private Object inlineCss;
    @SerializedName("url_strip_qs")
    @Expose
    private Object urlStripQs;
    @SerializedName("preserve_recipients")
    @Expose
    private Object preserveRecipients;
    @SerializedName("view_content_link")
    @Expose
    private Object viewContentLink;
    @SerializedName("bcc_address")
    @Expose
    private String bccAddress;
    @SerializedName("tracking_domain")
    @Expose
    private Object trackingDomain;
    @SerializedName("signing_domain")
    @Expose
    private Object signingDomain;
    @SerializedName("return_path_domain")
    @Expose
    private Object returnPathDomain;
    @SerializedName("merge")
    @Expose
    private Boolean merge;
    @SerializedName("merge_language")
    @Expose
    private String mergeLanguage;
    @SerializedName("global_merge_vars")
    @Expose
    private List<GlobalMergeVar> globalMergeVars = null;
    @SerializedName("merge_vars")
    @Expose
    private List<MergeVar> mergeVars = null;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("subaccount")
    @Expose
    private String subaccount;
    @SerializedName("google_analytics_domains")
    @Expose
    private List<String> googleAnalyticsDomains = null;
    @SerializedName("google_analytics_campaign")
    @Expose
    private String googleAnalyticsCampaign;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("recipient_metadata")
    @Expose
    private List<RecipientMetadatum> recipientMetadata = null;
    @SerializedName("attachments")
    @Expose
    private List<Attachment> attachments = null;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public List<To> getTo() {
        return to;
    }

    public void setTo(List<To> to) {
        this.to = to;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public Boolean getImportant() {
        return important;
    }

    public void setImportant(Boolean important) {
        this.important = important;
    }

    public Object getTrackOpens() {
        return trackOpens;
    }

    public void setTrackOpens(Object trackOpens) {
        this.trackOpens = trackOpens;
    }

    public Object getTrackClicks() {
        return trackClicks;
    }

    public void setTrackClicks(Object trackClicks) {
        this.trackClicks = trackClicks;
    }

    public Object getAutoText() {
        return autoText;
    }

    public void setAutoText(Object autoText) {
        this.autoText = autoText;
    }

    public Object getAutoHtml() {
        return autoHtml;
    }

    public void setAutoHtml(Object autoHtml) {
        this.autoHtml = autoHtml;
    }

    public Object getInlineCss() {
        return inlineCss;
    }

    public void setInlineCss(Object inlineCss) {
        this.inlineCss = inlineCss;
    }

    public Object getUrlStripQs() {
        return urlStripQs;
    }

    public void setUrlStripQs(Object urlStripQs) {
        this.urlStripQs = urlStripQs;
    }

    public Object getPreserveRecipients() {
        return preserveRecipients;
    }

    public void setPreserveRecipients(Object preserveRecipients) {
        this.preserveRecipients = preserveRecipients;
    }

    public Object getViewContentLink() {
        return viewContentLink;
    }

    public void setViewContentLink(Object viewContentLink) {
        this.viewContentLink = viewContentLink;
    }

    public String getBccAddress() {
        return bccAddress;
    }

    public void setBccAddress(String bccAddress) {
        this.bccAddress = bccAddress;
    }

    public Object getTrackingDomain() {
        return trackingDomain;
    }

    public void setTrackingDomain(Object trackingDomain) {
        this.trackingDomain = trackingDomain;
    }

    public Object getSigningDomain() {
        return signingDomain;
    }

    public void setSigningDomain(Object signingDomain) {
        this.signingDomain = signingDomain;
    }

    public Object getReturnPathDomain() {
        return returnPathDomain;
    }

    public void setReturnPathDomain(Object returnPathDomain) {
        this.returnPathDomain = returnPathDomain;
    }

    public Boolean getMerge() {
        return merge;
    }

    public void setMerge(Boolean merge) {
        this.merge = merge;
    }

    public String getMergeLanguage() {
        return mergeLanguage;
    }

    public void setMergeLanguage(String mergeLanguage) {
        this.mergeLanguage = mergeLanguage;
    }

    public List<GlobalMergeVar> getGlobalMergeVars() {
        return globalMergeVars;
    }

    public void setGlobalMergeVars(List<GlobalMergeVar> globalMergeVars) {
        this.globalMergeVars = globalMergeVars;
    }

    public List<MergeVar> getMergeVars() {
        return mergeVars;
    }

    public void setMergeVars(List<MergeVar> mergeVars) {
        this.mergeVars = mergeVars;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getSubaccount() {
        return subaccount;
    }

    public void setSubaccount(String subaccount) {
        this.subaccount = subaccount;
    }

    public List<String> getGoogleAnalyticsDomains() {
        return googleAnalyticsDomains;
    }

    public void setGoogleAnalyticsDomains(List<String> googleAnalyticsDomains) {
        this.googleAnalyticsDomains = googleAnalyticsDomains;
    }

    public String getGoogleAnalyticsCampaign() {
        return googleAnalyticsCampaign;
    }

    public void setGoogleAnalyticsCampaign(String googleAnalyticsCampaign) {
        this.googleAnalyticsCampaign = googleAnalyticsCampaign;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<RecipientMetadatum> getRecipientMetadata() {
        return recipientMetadata;
    }

    public void setRecipientMetadata(List<RecipientMetadatum> recipientMetadata) {
        this.recipientMetadata = recipientMetadata;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

}
