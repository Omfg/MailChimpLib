
package Objects.Messages.Parse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParseRequest {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("raw_message")
    @Expose
    private String rawMessage;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRawMessage() {
        return rawMessage;
    }

    public void setRawMessage(String rawMessage) {
        this.rawMessage = rawMessage;
    }

}
