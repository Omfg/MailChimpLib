
package Objects.Messages.Send_Template;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class To {
    private  To(Builder builder){
        this.name = builder.name;
        this.email = builder.email;
        this.type = builder.type;

    }

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;



    public static class Builder{
        public Builder(){

        }
        private String email;
        private String name;
        private String type;
        public String getEmail() {
            return email;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public String getName() {
            return name;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public String getType() {
            return type;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }
        public To build(){
            return new To(this);
        }
    }

}
