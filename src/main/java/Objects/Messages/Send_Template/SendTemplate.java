
package Objects.Messages.Send_Template;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendTemplate {
    private SendTemplate(Builder builder){
        this.key = builder.key;
        this.templateContent = builder.templateContent;
        this.templateName = builder.templateName;
        this.message = builder.message;
        this.async = builder.async;
        this.ipPool  = builder.ipPool;
        this.sendAt = builder.sendAt;
    }

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("template_name")
    @Expose
    private String templateName;
    @SerializedName("template_content")
    @Expose
    private List<TemplateContent> templateContent = null;
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("async")
    @Expose
    private Boolean async;
    @SerializedName("ip_pool")
    @Expose
    private String ipPool;
    @SerializedName("send_at")
    @Expose
    private String sendAt;


    public static class Builder  {


        private String key;
        private String templateName;
        private List<TemplateContent> templateContent = null;
        private Message message;
        private Boolean async;
        private String ipPool;
        private String sendAt;

        public Builder(){

        }
        public String getKey() {
            return key;
        }

        public Builder setKey(String key) {
            this.key = key;
            return this;
        }

        public String getTemplateName() {
            return templateName;
        }

        public Builder setTemplateName(String templateName) {
            this.templateName = templateName;
            return this;
        }

        public List<TemplateContent> getTemplateContent() {
            return templateContent;
        }

        public Builder setTemplateContent(List<TemplateContent> templateContent) {
            this.templateContent = templateContent;
            return this;
        }

        public Message getMessage() {
            return message;
        }

        public Builder setMessage(Message message) {
            this.message = message;
            return this;
        }

        public Boolean getAsync() {
            return async;
        }

        public Builder setAsync(Boolean async) {
            this.async = async;
            return this;
        }

        public String getIpPool() {
            return ipPool;
        }

        public Builder setIpPool(String ipPool) {
            this.ipPool = ipPool;
            return this;
        }

        public String getSendAt() {
            return sendAt;
        }

        public Builder setSendAt(String sendAt) {
            this.sendAt = sendAt;
            return this;
        }

        public SendTemplate build(){
             return new SendTemplate(this);

        }

    }
}
