
package Objects.Messages.Send_Template;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {
    private Message(Builder builder){
        this.html = builder.html;
        this.text = builder.text;
        this.subject = builder.subject;
        this.fromEmail = builder.fromEmail;
        this.fromName = builder.fromName;
        this.to = builder.to;
        this.headers = builder.headers;
        this.important = builder.important;
        this.trackOpens = builder.trackOpens;
        this.trackClicks = builder.trackClicks;
        this.autoText = builder.autoText;
        this.inlineCss = builder.inlineCss;
        this.urlStripQs =builder.urlStripQs;
        this.preserveRecipients = builder.preserveRecipients;
        this.viewContentLink  = builder.viewContentLink;
        this.bccAddress = builder.bccAddress;
        this.trackingDomain = builder.trackingDomain;
        this.signingDomain = builder.signingDomain;
        this.returnPathDomain = builder.returnPathDomain;
        this.merge = builder.merge;
        this.mergeLanguage = builder.mergeLanguage;
        this.globalMergeVars = builder.globalMergeVars;
        this.mergeVars = builder.mergeVars;
        this.tags = builder.tags;
        this.subaccount = builder.subaccount;
        this.googleAnalyticsDomains = builder.googleAnalyticsDomains;
        this.googleAnalyticsCampaign = builder.googleAnalyticsCampaign;
        this.metadata = builder.metadata;
        this.recipientMetadata = builder.recipientMetadata;
        this.attachments = builder.attachments;
        this.images = builder.images;

    }

    @SerializedName("html")
    @Expose
    private String html;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("from_email")
    @Expose
    private String fromEmail;
    @SerializedName("from_name")
    @Expose
    private String fromName;
    @SerializedName("to")
    @Expose
    private List<To> to = null;
    @SerializedName("headers")
    @Expose
    private Headers headers;
    @SerializedName("important")
    @Expose
    private Boolean important;
    @SerializedName("track_opens")
    @Expose
    private Object trackOpens;
    @SerializedName("track_clicks")
    @Expose
    private Object trackClicks;
    @SerializedName("auto_text")
    @Expose
    private Object autoText;
    @SerializedName("auto_html")
    @Expose
    private Object autoHtml;
    @SerializedName("inline_css")
    @Expose
    private Object inlineCss;
    @SerializedName("url_strip_qs")
    @Expose
    private Object urlStripQs;
    @SerializedName("preserve_recipients")
    @Expose
    private Object preserveRecipients;
    @SerializedName("view_content_link")
    @Expose
    private Object viewContentLink;
    @SerializedName("bcc_address")
    @Expose
    private String bccAddress;
    @SerializedName("tracking_domain")
    @Expose
    private Object trackingDomain;
    @SerializedName("signing_domain")
    @Expose
    private Object signingDomain;
    @SerializedName("return_path_domain")
    @Expose
    private Object returnPathDomain;
    @SerializedName("merge")
    @Expose
    private Boolean merge;
    @SerializedName("merge_language")
    @Expose
    private String mergeLanguage;
    @SerializedName("global_merge_vars")
    @Expose
    private List<GlobalMergeVar> globalMergeVars = null;
    @SerializedName("merge_vars")
    @Expose
    private List<MergeVar> mergeVars = null;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("subaccount")
    @Expose
    private String subaccount;
    @SerializedName("google_analytics_domains")
    @Expose
    private List<String> googleAnalyticsDomains = null;
    @SerializedName("google_analytics_campaign")
    @Expose
    private String googleAnalyticsCampaign;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("recipient_metadata")
    @Expose
    private List<RecipientMetadatum> recipientMetadata = null;
    @SerializedName("attachments")
    @Expose
    private List<Attachment> attachments = null;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;


    public static class Builder{
        private String html;
        private String text;
        private String subject;
        private String fromEmail;
        private String fromName;
        private List<To> to = null;
        private Headers headers;
        private Boolean important;
        private Object trackOpens;
        private Object trackClicks;
        private Object autoText;
        private Object autoHtml;
        private Object inlineCss;
        private Object urlStripQs;
        private Object preserveRecipients;
        private Object viewContentLink;
        private String bccAddress;
        private Object trackingDomain;
        private Object signingDomain;
        private Object returnPathDomain;
        private Boolean merge;
        private String mergeLanguage;
        private List<GlobalMergeVar> globalMergeVars = null;
        private List<MergeVar> mergeVars = null;
        private List<String> tags = null;
        private String subaccount;
        private List<String> googleAnalyticsDomains = null;
        private String googleAnalyticsCampaign;
        private Metadata metadata;
        private List<RecipientMetadatum> recipientMetadata = null;
        private List<Attachment> attachments = null;
        private List<Image> images = null;

        public Builder(){

        }
        public String getHtml() {
            return html;
        }

        public Builder setHtml(String html) {
            this.html = html;
            return this;
        }

        public String getText() {
            return text;
        }

        public Builder setText(String text) {
            this.text = text;
            return this;
        }

        public String getSubject() {
            return subject;
        }

        public Builder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public String getFromEmail() {
            return fromEmail;
        }

        public Builder setFromEmail(String fromEmail) {
            this.fromEmail = fromEmail;
            return this;
        }

        public String getFromName() {
            return fromName;
        }

        public Builder  setFromName(String fromName) {
            this.fromName = fromName;
            return this;
        }

        public List<To> getTo() {
            return to;
        }

        public Builder setTo(List<To> to) {
            this.to = to;
            return this;
        }

        public Headers getHeaders() {
            return headers;
        }

        public Builder setHeaders(Headers headers) {
            this.headers = headers;
            return this;
        }

        public Boolean getImportant() {
            return important;
        }

        public Builder setImportant(Boolean important) {
            this.important = important;
            return this;
        }

        public Object getTrackOpens() {
            return trackOpens;
        }

        public Builder setTrackOpens(Object trackOpens) {
            this.trackOpens = trackOpens;
            return this;
        }

        public Object getTrackClicks() {
            return trackClicks;
        }

        public Builder setTrackClicks(Object trackClicks) {
            this.trackClicks = trackClicks;
            return this;
        }

        public Object getAutoText() {
            return autoText;
        }

        public Builder setAutoText(Object autoText) {
            this.autoText = autoText;
            return this;
        }

        public Object getAutoHtml() {
            return autoHtml;
        }

        public Builder setAutoHtml(Object autoHtml) {
            this.autoHtml = autoHtml;
            return this;
        }

        public Object getInlineCss() {
            return inlineCss;
        }

        public Builder setInlineCss(Object inlineCss) {
            this.inlineCss = inlineCss;
            return this;
        }

        public Object getUrlStripQs() {
            return urlStripQs;
        }

        public Builder setUrlStripQs(Object urlStripQs) {
            this.urlStripQs = urlStripQs;
            return this;
        }

        public Object getPreserveRecipients() {
            return preserveRecipients;
        }

        public Builder setPreserveRecipients(Object preserveRecipients) {
            this.preserveRecipients = preserveRecipients;
            return this;
        }

        public Object getViewContentLink() {
            return viewContentLink;
        }

        public Builder setViewContentLink(Object viewContentLink) {
            this.viewContentLink = viewContentLink;
            return this;
        }

        public String getBccAddress() {
            return bccAddress;
        }

        public Builder setBccAddress(String bccAddress) {
            this.bccAddress = bccAddress;
            return this;
        }

        public Object getTrackingDomain() {
            return trackingDomain;
        }

        public Builder setTrackingDomain(Object trackingDomain) {
            this.trackingDomain = trackingDomain;
            return this;
        }

        public Object getSigningDomain() {
            return signingDomain;
        }

        public Builder setSigningDomain(Object signingDomain) {
            this.signingDomain = signingDomain;
            return this;
        }

        public Object getReturnPathDomain() {
            return returnPathDomain;
        }

        public Builder setReturnPathDomain(Object returnPathDomain) {
            this.returnPathDomain = returnPathDomain;
            return this;
        }

        public Boolean getMerge() {
            return merge;
        }

        public Builder setMerge(Boolean merge) {
            this.merge = merge;
            return this;
        }

        public String getMergeLanguage() {
            return mergeLanguage;
        }

        public Builder setMergeLanguage(String mergeLanguage) {
            this.mergeLanguage = mergeLanguage;
            return this;
        }

        public List<GlobalMergeVar> getGlobalMergeVars() {
            return globalMergeVars;
        }

        public Builder setGlobalMergeVars(List<GlobalMergeVar> globalMergeVars) {
            this.globalMergeVars = globalMergeVars;
            return this;
        }

        public List<MergeVar> getMergeVars() {
            return mergeVars;
        }

        public Builder setMergeVars(List<MergeVar> mergeVars) {
            this.mergeVars = mergeVars;
            return this;
        }

        public List<String> getTags() {
            return tags;
        }

        public Builder setTags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public String getSubaccount() {
            return subaccount;
        }

        public Builder setSubaccount(String subaccount) {
            this.subaccount = subaccount;
            return this;
        }

        public List<String> getGoogleAnalyticsDomains() {
            return googleAnalyticsDomains;
        }

        public Builder setGoogleAnalyticsDomains(List<String> googleAnalyticsDomains) {
            this.googleAnalyticsDomains = googleAnalyticsDomains;
            return this;
        }

        public String getGoogleAnalyticsCampaign() {
            return googleAnalyticsCampaign;
        }

        public Builder setGoogleAnalyticsCampaign(String googleAnalyticsCampaign) {
            this.googleAnalyticsCampaign = googleAnalyticsCampaign;
            return this;

        }

        public Metadata getMetadata() {
            return metadata;
        }

        public Builder setMetadata(Metadata metadata) {
            this.metadata = metadata;
            return this;
        }

        public List<RecipientMetadatum> getRecipientMetadata() {
            return recipientMetadata;
        }

        public Builder setRecipientMetadata(List<RecipientMetadatum> recipientMetadata) {
            this.recipientMetadata = recipientMetadata;
            return this;
        }

        public List<Attachment> getAttachments() {
            return attachments;
        }

        public Builder setAttachments(List<Attachment> attachments) {
            this.attachments = attachments;
            return this;
        }

        public List<Image> getImages() {
            return images;
        }

        public Builder setImages(List<Image> images) {
            this.images = images;
            return this;
        }


        public Message build(){
            return new Message(this);
        }
    }

}
