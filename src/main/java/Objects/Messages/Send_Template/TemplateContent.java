
package Objects.Messages.Send_Template;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TemplateContent {
    private TemplateContent(Builder builder){
        this.name = builder.name;
        this.content = builder.content;

    }

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("content")
    @Expose
    private String content;



    public static class Builder {
        private String name;
        private String content;

        public String getName() {
            return name;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public String getContent() {
            return content;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }
        public TemplateContent build(){
            return new TemplateContent(this);
        }
    }
}
