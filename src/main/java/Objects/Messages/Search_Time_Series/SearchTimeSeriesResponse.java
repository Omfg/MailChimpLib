
package Objects.Messages.Search_Time_Series;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchTimeSeriesResponse {

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("sent")
    @Expose
    private Integer sent;
    @SerializedName("hard_bounces")
    @Expose
    private Integer hardBounces;
    @SerializedName("soft_bounces")
    @Expose
    private Integer softBounces;
    @SerializedName("rejects")
    @Expose
    private Integer rejects;
    @SerializedName("complaints")
    @Expose
    private Integer complaints;
    @SerializedName("unsubs")
    @Expose
    private Integer unsubs;
    @SerializedName("opens")
    @Expose
    private Integer opens;
    @SerializedName("unique_opens")
    @Expose
    private Integer uniqueOpens;
    @SerializedName("clicks")
    @Expose
    private Integer clicks;
    @SerializedName("unique_clicks")
    @Expose
    private Integer uniqueClicks;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getSent() {
        return sent;
    }

    public void setSent(Integer sent) {
        this.sent = sent;
    }

    public Integer getHardBounces() {
        return hardBounces;
    }

    public void setHardBounces(Integer hardBounces) {
        this.hardBounces = hardBounces;
    }

    public Integer getSoftBounces() {
        return softBounces;
    }

    public void setSoftBounces(Integer softBounces) {
        this.softBounces = softBounces;
    }

    public Integer getRejects() {
        return rejects;
    }

    public void setRejects(Integer rejects) {
        this.rejects = rejects;
    }

    public Integer getComplaints() {
        return complaints;
    }

    public void setComplaints(Integer complaints) {
        this.complaints = complaints;
    }

    public Integer getUnsubs() {
        return unsubs;
    }

    public void setUnsubs(Integer unsubs) {
        this.unsubs = unsubs;
    }

    public Integer getOpens() {
        return opens;
    }

    public void setOpens(Integer opens) {
        this.opens = opens;
    }

    public Integer getUniqueOpens() {
        return uniqueOpens;
    }

    public void setUniqueOpens(Integer uniqueOpens) {
        this.uniqueOpens = uniqueOpens;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getUniqueClicks() {
        return uniqueClicks;
    }

    public void setUniqueClicks(Integer uniqueClicks) {
        this.uniqueClicks = uniqueClicks;
    }

}
