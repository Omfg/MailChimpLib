
package Objects.Messages.Reshedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResheduleRequest {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("send_at")
    @Expose
    private String sendAt;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

}
