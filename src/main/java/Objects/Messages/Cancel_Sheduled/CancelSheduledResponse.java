
package Objects.Messages.Cancel_Sheduled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelSheduledResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("send_at")
    @Expose
    private String sendAt;
    @SerializedName("from_email")
    @Expose
    private String fromEmail;
    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("subject")
    @Expose
    private String subject;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
