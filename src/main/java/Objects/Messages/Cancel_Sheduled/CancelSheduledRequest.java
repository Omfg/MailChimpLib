
package Objects.Messages.Cancel_Sheduled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelSheduledRequest {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("id")
    @Expose
    private Object id;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

}
