
package Objects.Messages.Send_Raw;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendRawRequest {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("raw_message")
    @Expose
    private String rawMessage;
    @SerializedName("from_email")
    @Expose
    private String fromEmail;
    @SerializedName("from_name")
    @Expose
    private String fromName;
    @SerializedName("to")
    @Expose
    private List<String> to = null;
    @SerializedName("async")
    @Expose
    private Boolean async;
    @SerializedName("ip_pool")
    @Expose
    private String ipPool;
    @SerializedName("send_at")
    @Expose
    private String sendAt;
    @SerializedName("return_path_domain")
    @Expose
    private Object returnPathDomain;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRawMessage() {
        return rawMessage;
    }

    public void setRawMessage(String rawMessage) {
        this.rawMessage = rawMessage;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public Boolean getAsync() {
        return async;
    }

    public void setAsync(Boolean async) {
        this.async = async;
    }

    public String getIpPool() {
        return ipPool;
    }

    public void setIpPool(String ipPool) {
        this.ipPool = ipPool;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

    public Object getReturnPathDomain() {
        return returnPathDomain;
    }

    public void setReturnPathDomain(Object returnPathDomain) {
        this.returnPathDomain = returnPathDomain;
    }

}
