
package Objects.Messages.Send_Raw;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendRawResponse {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reject_reason")
    @Expose
    private String rejectReason;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
