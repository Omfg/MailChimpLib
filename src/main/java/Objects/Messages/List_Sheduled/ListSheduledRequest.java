
package Objects.Messages.List_Sheduled;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListSheduledRequest {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("to")
    @Expose
    private String to;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
