
package Objects.Messages.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClicksDetail {

    @SerializedName("ts")
    @Expose
    private Integer ts;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("ua")
    @Expose
    private String ua;

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

}
