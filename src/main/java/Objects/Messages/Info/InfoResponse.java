
package Objects.Messages.Info;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfoResponse {

    @SerializedName("ts")
    @Expose
    private Integer ts;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("sender")
    @Expose
    private String sender;
    @SerializedName("template")
    @Expose
    private String template;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("opens")
    @Expose
    private Integer opens;
    @SerializedName("opens_detail")
    @Expose
    private List<OpensDetail> opensDetail = null;
    @SerializedName("clicks")
    @Expose
    private Integer clicks;
    @SerializedName("clicks_detail")
    @Expose
    private List<ClicksDetail> clicksDetail = null;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("smtp_events")
    @Expose
    private List<SmtpEvent> smtpEvents = null;

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Integer getOpens() {
        return opens;
    }

    public void setOpens(Integer opens) {
        this.opens = opens;
    }

    public List<OpensDetail> getOpensDetail() {
        return opensDetail;
    }

    public void setOpensDetail(List<OpensDetail> opensDetail) {
        this.opensDetail = opensDetail;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public List<ClicksDetail> getClicksDetail() {
        return clicksDetail;
    }

    public void setClicksDetail(List<ClicksDetail> clicksDetail) {
        this.clicksDetail = clicksDetail;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<SmtpEvent> getSmtpEvents() {
        return smtpEvents;
    }

    public void setSmtpEvents(List<SmtpEvent> smtpEvents) {
        this.smtpEvents = smtpEvents;
    }

}
