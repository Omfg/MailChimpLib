
package Objects.Messages.Info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfoRequest {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("id")
    @Expose
    private String id;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
