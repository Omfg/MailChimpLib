
package Objects.Messages.Info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmtpEvent {

    @SerializedName("ts")
    @Expose
    private Integer ts;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("diag")
    @Expose
    private String diag;

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDiag() {
        return diag;
    }

    public void setDiag(String diag) {
        this.diag = diag;
    }

}
