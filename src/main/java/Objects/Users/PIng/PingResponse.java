
package Objects.Users.PIng;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PingResponse {

    @SerializedName("PING")
    @Expose
    private String pING;

    public String getPING() {
        return pING;
    }

    public void setPING(String pING) {
        this.pING = pING;
    }

}
