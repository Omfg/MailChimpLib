
package Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserInfoResponse {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("public_id")
    @Expose
    private String publicId;
    @SerializedName("reputation")
    @Expose
    private Integer reputation;
    @SerializedName("hourly_quota")
    @Expose
    private Integer hourlyQuota;
    @SerializedName("backlog")
    @Expose
    private Integer backlog;
    @SerializedName("stats")
    @Expose
    private Objects.Stats stats;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Integer getReputation() {
        return reputation;
    }

    public void setReputation(Integer reputation) {
        this.reputation = reputation;
    }

    public Integer getHourlyQuota() {
        return hourlyQuota;
    }

    public void setHourlyQuota(Integer hourlyQuota) {
        this.hourlyQuota = hourlyQuota;
    }

    public Integer getBacklog() {
        return backlog;
    }

    public void setBacklog(Integer backlog) {
        this.backlog = backlog;
    }

    public Objects.Stats getStats() {
        return stats;
    }

    public void setStats(Objects.Stats stats) {
        this.stats = stats;
    }






}
