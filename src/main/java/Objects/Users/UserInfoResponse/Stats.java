
package Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stats {

    @SerializedName("today")
    @Expose
    private Today today;
    @SerializedName("last_7_days")
    @Expose
    private Last7Days last7Days;
    @SerializedName("last_30_days")
    @Expose
    private Last30Days last30Days;
    @SerializedName("last_60_days")
    @Expose
    private Last60Days last60Days;
    @SerializedName("last_90_days")
    @Expose
    private Last90Days last90Days;
    @SerializedName("all_time")
    @Expose
    private AllTime allTime;

    public Today getToday() {
        return today;
    }

    public void setToday(Today today) {
        this.today = today;
    }

    public Last7Days getLast7Days() {
        return last7Days;
    }

    public void setLast7Days(Last7Days last7Days) {
        this.last7Days = last7Days;
    }

    public Last30Days getLast30Days() {
        return last30Days;
    }

    public void setLast30Days(Last30Days last30Days) {
        this.last30Days = last30Days;
    }

    public Last60Days getLast60Days() {
        return last60Days;
    }

    public void setLast60Days(Last60Days last60Days) {
        this.last60Days = last60Days;
    }

    public Last90Days getLast90Days() {
        return last90Days;
    }

    public void setLast90Days(Last90Days last90Days) {
        this.last90Days = last90Days;
    }

    public AllTime getAllTime() {
        return allTime;
    }

    public void setAllTime(AllTime allTime) {
        this.allTime = allTime;
    }

}
