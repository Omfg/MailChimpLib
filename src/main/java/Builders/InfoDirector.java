package Builders;

import Objects.AuthObj;

public class InfoDirector {
    private InfoBuilder infoBuilder;
    public void setInfoBuilder(InfoBuilder infoBuilder){
        this.infoBuilder = infoBuilder;
    }
    public AuthObj getAuthObj(){
        return infoBuilder.getAuthObj();
    }
    public void constructAuthObj(){
        infoBuilder.createNewAuthObj();
        infoBuilder.buildAuthObj();

    }
}
