package Builders.MessagesBuilders.SendTemplate;

import Objects.Messages.Send_Template.TemplateContent;

public abstract class TemplateContentBuilder {
    private TemplateContent templateContent;

    public TemplateContent getTemplateContent() {
        return templateContent;

    }

    public void setTemplateContent(TemplateContent templateContent) {
        this.templateContent = templateContent;

    }

    public abstract void buildName();
    public abstract void buildContent();

}
