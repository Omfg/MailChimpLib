package Builders.MessagesBuilders.SendTemplate;

import Objects.Messages.Send_Template.SendTemplate;
import Objects.Messages.Send_Template.TemplateContent;

import java.util.List;

public abstract class SendTemplateBuilder {

    protected SendTemplate sendTemplate;

    protected List<TemplateContent> templateContents;

    public void setSendTemplate(SendTemplate sendTemplate) {
        this.sendTemplate = sendTemplate;
    }

    public SendTemplate getSendTemplate() {

        return sendTemplate;
    }

    public abstract void buildKey();

    public abstract void buildTemplateName();

    public abstract void buildMessage();

    public abstract void buildAsync();

    public abstract void buildIpPool();
}
