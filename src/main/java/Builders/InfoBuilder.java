package Builders;

import Objects.AuthObj;
import Objects.UserInfoResponse;

public abstract class InfoBuilder {
    protected AuthObj authObj;

    public AuthObj getAuthObj() {
        return authObj;
    }
    public void createNewAuthObj(){
        authObj = new AuthObj();
    }

    public abstract void buildAuthObj();
}
