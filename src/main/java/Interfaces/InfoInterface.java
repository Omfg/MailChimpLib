package Interfaces;

import Objects.AuthObj;
import Objects.Users.Senders.SendersResponse;
import Objects.UserInfoResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

public interface InfoInterface {
    @POST("/api/1.0/users/info.json")

//    Call<UserInfoResponse> getInfo(@Body AuthObj authObj);
    Call<UserInfoResponse> getInfo(@Body AuthObj authObj);

    @POST("/api/1.0/users/senders.json")
    Call<List<SendersResponse>> getSenders(@Body AuthObj authObj);
}
