package Interfaces;

import Objects.MandrillCredentials;
import Objects.Messages.Send_Request.SendClass;

public interface SenderInterface {
    public void send(MandrillCredentials mandrillCredentials, SendClass sendClass);
}
