package Interfaces;

import Objects.AuthObj;
import Objects.Users.PIng.PingResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PingInteface {
    @POST("api/1.0/users/ping2.json")
    Call<PingResponse> getPing(@Body AuthObj authObj);
}
