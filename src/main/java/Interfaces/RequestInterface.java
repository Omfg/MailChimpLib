package Interfaces;

import Objects.MandrillCredentials;

public interface RequestInterface {
    public void request(MandrillCredentials mandrillCredentials, Object obj);
}
