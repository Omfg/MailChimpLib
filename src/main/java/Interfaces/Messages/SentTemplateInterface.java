package Interfaces.Messages;

import Objects.Messages.Send_Template.SendTemplate;
import Objects.Messages.Send_Template.SendTemplateResponseObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

public interface SentTemplateInterface {
    @POST("/api/1.0/messages/send-template.json")
    Call<List<SendTemplateResponseObject>> getSendTemplate(@Body SendTemplate sendTemplate);
}
