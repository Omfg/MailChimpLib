package Interfaces.Messages;

import Objects.Messages.SendResponse.MessageSendResponse;
import Objects.Messages.Send_Request.SendClass;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.List;

public interface MessageSenderInterface {
    @POST("/api/1.0/messages/send.json")
    Call<List<MessageSendResponse>> getMessageSend(@Body SendClass sendClass);

}
